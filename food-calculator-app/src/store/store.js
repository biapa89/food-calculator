import Vue from 'vue';
import Vuex from 'vuex';
const fb = require('../config/firebaseConfig')

Vue.use(Vuex);

fb.auth.onAuthStateChanged(user => {
    if (user) {
        store.commit('setCurrentUser', user)
        store.dispatch('fetchUserProfile')
        

        fb.usersCollection.doc(user.uid).onSnapshot(doc => {
            store.commit('setUserProfile', doc.data())
            store.dispatch('getRecipes')
            store.dispatch('getShoppingList')
            console.log(store.state.itemsList)
        })
    }
})

export const store = new Vuex.Store({
    state: {
        currentUser: null,
        userProfile: {},
        recipes: [],
        itemsList: [],
    },
    actions: {

        clearData({ commit }) {
            commit('setCurrentUser', null)
            commit('setUserProfile', null)
        },
        fetchUserProfile({ commit, state }) {
            fb.usersCollection.doc(state.currentUser.uid).get().then(res => {
                commit('setUserProfile', res.data())
                this.dispatch('getRecipes')
                this.dispatch('getShoppingList')
            }).catch(err => {
                console.log(err)
            })
        },
        getRecipes(){
            let recipeArray = [];
            fb.db
              .collection("usersRecipes")
              .doc(store.state.currentUser.uid)
              .collection("recipe")
              .get()
              .then(snapshot => {
                snapshot.docs.map(doc => {
                  recipeArray.push(doc.data());
                  store.state.recipes = recipeArray;
                });
              });
        },
        getShoppingList(){
            let shoppingArray = [];
            fb.db
              .collection("shoppingList")
              .doc(store.state.currentUser.uid)
              .get()
              .then(snapshot => {
                  shoppingArray.push(snapshot.data());
                  this.state.itemsList = shoppingArray[0].itemsList;
                  console.log(this.itemsList)
              });
          }
    },
    
    mutations: {
        setCurrentUser(state, val) {
            state.currentUser = val

        },
        setUserProfile(state, val) {
            state.userProfile = val

        },
        


    },
    computed: {

    }
})