//  import firebase from 'firebase';
import Vue from 'vue';
import Router from 'vue-router';
const fb = require('./config/firebaseConfig')

import Login from './views/Login.vue';
import SignUp from './views/SignUp.vue';
import Home from './views/Home.vue';
import UserProfile from './views/UserProfile.vue';
import Calorie from './views/Calorie.vue'
import ShoppingList from './views/ShoppingList.vue'
import MyRecipes from './views/MyRecipes'

Vue.use(Router);

const router = new Router({
    mode: 'history',
    routes: [{
            path: '*',
            redirect: 'user-profile'
        },
        {
            path: '/',
            name: 'Home',
            component: Home,
        },
        {
            path: '/login',
            name: 'Login',
            component: Login,
        },
        {
            path: '/sign-up',
            name: 'SignUp',
            component: SignUp,
        },
        {
            path: '/Kalorienrechner',
            name: 'Calorie',
            component: Calorie,
        },
        {
            path: '/user-profile',
            name: 'UserProfile',
            component: UserProfile,
            // meta: {
            //     requiresAuth: true,
            // },
        },
        {
            path: '/shopping-list',
            name: 'ShoppingList',
            component: ShoppingList,
        },
        {
            path: '/my-recipes',
            name: 'MyRecipes',
            component: MyRecipes,
        },
    ]
});

router.beforeEach((to, from, next) => {
    const currentUser = fb.currentUser;
    const requiresAuth = to.matched.some(record => record.meta.requiresAuth);

    if (requiresAuth && !currentUser) {
        next({ name: 'Login' });
    } else if (!requiresAuth && currentUser) {
        next();
    } else next()
})

export default router