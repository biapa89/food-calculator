import firebase from 'firebase';
import 'firebase/firestore'

  var firebaseConfig = {
    apiKey: "AIzaSyAQHj-qHQUISB7rx4qrXNE-CTkgL8U5EGI",
    authDomain: "food-calculator-2c98a.firebaseapp.com",
    databaseURL: "https://food-calculator-2c98a.firebaseio.com",
    projectId: "food-calculator-2c98a",
    storageBucket: "food-calculator-2c98a.appspot.com",
    messagingSenderId: "728392624025",
    appId: "1:728392624025:web:33065886511aecf3761508"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  
  // firebase utils
  const db = firebase.firestore();
  const auth = firebase.auth();
  const currentUser = auth.currentUser;

  //  firebase.firestore.setLogLevel('debug')

  const usersCollection = db.collection('users')

export {
  db,
  auth,
  currentUser,
  usersCollection,
}

