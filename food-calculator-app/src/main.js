import Vue from 'vue';
import App from './App.vue';
import router from './router'
import { store } from './store/store.js'

const fb = require('./config/firebaseConfig.js')
Vue.config.devtools = true;

Vue.config.productionTip = false


// handle page reloads 
//  new Vue({
//         el: '#app',
//         store,
//         router,
//         methods:{
//           getUnits:()=> console.log(fb.currentUser)
//         },
//         beforeMount(){
//           this.getUnits()
//        },
//         render: h => h(App),
//       }).$mount('#app')


let app;
fb.auth.onAuthStateChanged((user) => {
    if (!app) {
        //app = new Vue({
        new Vue({
            router,
            store,
            render: (h) => h(App),
        }).$mount('#app');
    }
    console.log(user)
});